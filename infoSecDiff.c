//  infoSecDiff.c
//  
//
//  Created by Luca Carcano on 08/02/15.

#include <stdio.h>
#include <stdlib.h>
#include <strings.h>

struct line
{
    char* MD5;
    char* SHA;
    char* hashFilePath;
};

void hashReader(struct line  **machineStructure, int lineNum, FILE *inputFile)
{
    int i=0;
    char *str1[32];
    char *str2[40];
    char *str3[500];
    
    for(i=0; i<lineNum; i++)
    {
        fscanf(inputFile, "%s\t%s\t%s", &str1, &str2, &str3);
        
        machineStructure[i]=(struct line*)malloc(sizeof(struct line));
        
        //memory allocation for the strings and assignment oif the values
        machineStructure[i]->MD5 = malloc((strlen(str1)+1)*sizeof(char));
        strncpy(machineStructure[i]->MD5,   str1, strlen(str1));
        
        machineStructure[i]->hashFilePath = malloc((strlen(str3)+1)*sizeof(char));
        strncpy(machineStructure[i]->hashFilePath,  str3, strlen(str3));
    }

    fclose(inputFile);
}


void hashChecker(struct line** base,struct  line **infect, int baseNum, int infectedNum)
{
    int i,j=0;

    int flag = 0;
    
    FILE *outputfile = fopen("outputfile.txt","w+");
    
    //external loop for the (longer) infected file, inner loop for the shorter original
    
    for (i=0; i<infectedNum; i++)
    {
        for (j=0; j<baseNum; j++)
        {
            if (strcmp(infect[i]->hashFilePath, base[j]->hashFilePath)==0) //if the path is the same i compare the hashes
            {
                //printf("comparing %s and %s\n",infect[i]->hashFilePath, base[j]->hashFilePath);
                
                flag = 1;
                
                if (strcmp(infect[i]->MD5, base[j]->MD5)!=0) //if the hashes are different i highlight
                {
                    //printf("diverso\n");

                    fprintf(outputfile, "Difference found at %d %s\nHashes:\n%s\n%s\n\n", i, infect[i]->hashFilePath,infect[i]->MD5,base[j]->MD5);
                }
            }
            
            //printf("flag = %d\n", flag);
        }
        
        if (flag!=1) //flag becomes 1 if the line matches
        {
            fprintf(outputfile, "New line at %d, path %s\n\n", i, infect[i]->hashFilePath);
            flag = 0;
        }
        
        flag = 0; //flag is set back to 0
    }
    
    fclose(outputfile);
}

int main(int argc, char* argv[])
{
    
    if (!argc)
        return 0;
    
    int baseLineNum;
    int cloneLineNum;
    int i;
    
    FILE *BaseMachineFile = fopen(argv[1], "r");
    FILE *InfectedMachineFile = fopen(argv[2], "r");
    
    fscanf(BaseMachineFile, "%d", &baseLineNum);
    fscanf(InfectedMachineFile, "%d", &cloneLineNum);
    
    printf("%d, %d\n", baseLineNum, cloneLineNum);
    
    struct line **baseMachineComplete  = (struct line**)malloc((baseLineNum)   * sizeof(struct line*));
    struct line **cloneMachineComplete = (struct line**)malloc((cloneLineNum)  * sizeof(struct line*));

    //initialize structure

    /*
    for (i=0; i<baseLineNum; i++)
    {
        baseMachineComplete[i] = (struct line*)malloc(sizeof(struct line));
    }
    for (i=0; i<cloneLineNum; i++)
    {
        cloneMachineComplete[i] = (struct line*)malloc(sizeof(struct line));
    }*/
    
    //initialize structures and fill them in
    
    hashReader(baseMachineComplete, baseLineNum, BaseMachineFile);
    hashReader(cloneMachineComplete, cloneLineNum, InfectedMachineFile);
    
    //checks the hashes
    
    hashChecker(baseMachineComplete, cloneMachineComplete, baseLineNum, cloneLineNum);
    
    //frees memory
    
    free(baseMachineComplete);
    free(cloneMachineComplete);
    
    return 0;
    

}